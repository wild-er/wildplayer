# Display (tiled) images on Cluster-Driven Ultra-high-resolution Wall Displays



## Installation

In both `server` and `client` directories, run:

```npm install```


Make sure that the serve module is installed on both the master and slave computers:

```npm install -g serve```

Make sure the pm2 module is installed on the master computer:

```npm install -g pm2```

## Usage on Wilder platform

* Launch sync and http server using pm2

```./WilderLaunchServer.sh ```

* Launch NW.js on the cluster

```./WilderLaunchCluster.sh ```

* Stop NW.js on the cluster

```walldo killall electron ```

* Stop sync and tile server using pm2

```./WilderStopServer.sh ```

* Choose image by filling the url in the textfield on the master
