#!/bin/bash

#connexion prameters
serverip="127.0.0.1"
viewSyncPort=8080
tuioport=8083
shipUpdatePort=8084
serverport=8081

#client offset in pixel
offsetX=3057
offsetY=864
#client screen size
clientscreenwidth=1536
clientscreenheight=432

#master screen size
masterscreenwidth=1536
masterscreenheight=432


#wall total size
wallwidth=6114
wallheight=1728


echo "start nw client"

echo "Client web page:" http://$serverip:$serverport/index.html?id=a1\&offsetx=$offsetX\&offsety=$offsetY\&wallwidth=$wallwidth\&wallheight=$wallheight\&sizex=$clientscreenwidth\&sizey=$clientscreenheight\&syncserver=$serverip\&syncport=$viewSyncPort

echo "Master web page:" http://$serverip:$serverport/index.html?id=MASTER\&sizex=$masterscreenwidth\&sizey=$masterscreenheight\&wallwidth=$wallwidth\&wallheight=$wallheight\&viewSyncPort=$viewSyncPort\&tuioserver=$serverip\&tuioport=$tuioport
