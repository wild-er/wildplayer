viewSyncPort=8080
serverport=8081
serverip="192.168.0.2"


source ~/.nvm/nvm.sh
nvm use v17.2.0

echo "start node wildsyncserver for sync & tuio"
cd ../server && pm2 start --name "wildsyncserver" ./node_modules/wildsyncserver/lib/server.js -- --ip=$serverip

echo "start http server locally on port "$serverport
cd ../client && pm2 start npm --name "wildplayerhttp" -- run http
