
offsetX=0
offsetY=0
wcol=4
wrow=4
wtile_w=15360
wtile_h=4320
viewSyncPort=8080
serverport=8081
tileport=8082
tuioport=8083
nwpath="/home/wild/.nvm/versions/node/v17.2.0/bin/nw"
user=wild
wallwidth=61440
wallheight=17280
#proxyaddress="192.168.2.254:8086"

#master canvas size. MUST be the same ratio than the wall
masterwidth=2456
masterheight=688


packagefile="package.json"
nodetmppath="/home/wild/tmp/wildplayer"
nodeuserdirpath="/home/wild/.config/wildplayer"

packagefile="package.json"

servertmppath="/home/wild/tmp/wildplayer/"
serverpackagepath=$servertmppath$packagefile

serverip="192.168.0.2"
tileserverip="127.0.0.1"

clientcachedir="/home/wild/tmp/wildplayer"

# 16-level pyramid using ESRI below 8


echo "Remove the decoration for all the window with a name that start with Wild"
walldo DISPLAY=:0.0 FvwmCommand -i0 "Style Wild* NoTitle, BorderWidth 0, NoHandles"



echo "clear nw cache"

walldo rm -r $nodeuserdirpath
walldo rm -r $nodetmppath
walldo rm -r $clientcachedir
sleep 1
walldo mkdir $nodetmppath
walldo mkdir $clientcachedir

sleep 1

echo "start nw on nodes"
for col in {a..d}
do
  for row in {1..4}
    do
      echo  {\"name\": \"WildPlayer\",\"nodejs\": \"true\",\"main\": "\"http://$serverip:$serverport/?id=$col$row&offsetx=$offsetX&offsety=$offsetY&wallx=$wcol&wally=$wrow&wallwidth=$wallwidth&wallheight=$wallheight&sizex=$wtile_w&sizey=$wtile_h&syncserver=$serverip&viewSyncPort=$viewSyncPort\"",\"window\": {\"kiosk\": false,\"width\": $wtile_w,\"height\": $wtile_h},\"chromium-args\": \"--enable-file-cookies --allow-running-insecure-content --ignore-certificate-errors\"} > $serverpackagepath$col$row
      scp $serverpackagepath$col$row $user@$col$row:$nodetmppath/$packagefile
      ssh $user@$col$row DISPLAY=:0 $nwpath $nodetmppath --disk-cache-dir=$clientcachedir&
      sleep 0.2
      offsetY=$((offsetY+wtile_h))
    done
    offsetX=$((offsetX+wtile_w))
    offsetY=0
done


sleep 2

echo  {\"name\": \"WildPlayer\",\"nodejs\": \"true\",\"main\": "\"http://$serverip:$serverport/?id=MASTER&wallwidth=$wallwidth&wallheight=$wallheight&sizex=$masterwidth&sizey=$masterheight&syncserver=$serverip&viewSyncPort=$viewSyncPort\"",\"window\": {\"kiosk\": false,\"width\": $masterwidth,\"height\": $masterheight},\"chromium-args\": \"--enable-file-cookies --allow-running-insecure-content --ignore-certificate-errors\"} > $serverpackagepath
nw $servertmppath
