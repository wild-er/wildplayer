
offsetX=0
offsetY=0
wcol=2
wrow=5
wtile_w=14400
wtile_h=960
viewSyncPort=8080
serverport=8081
tileport=8082
tuioport=8083
electronpath="electron"
electronstartscript="electronmain.js"
electronpreloadscript="preload.js"
user=wild
wallwidth=14400
wallheight=4800
#proxyaddress="192.168.2.254:8086"

#master canvas size. MUST be the same ratio than the wall
masterwidth=1440
masterheight=480
masterTotalHeight=600



packagefile="package.json"
nodetmppath="/home/wild/tmp/wildplayer"
nodeuserdirpath="/home/wild/.config/wildplayer"

packagefile="package.json"

servertmppath="/home/wild/tmp/wildplayer/"
serverpackagepath=$servertmppath$packagefile

serverip="192.168.2.5"
tileserverip="127.0.0.1"

clientcachedir="/home/wild/tmp/wildplayer/"




# echo "clear nw cache"

# walldo rm -r $nodeuserdirpath
# walldo rm -r $nodetmppath
# walldo rm -r $clientcachedir
# sleep 1
# walldo mkdir $nodetmppath
# walldo mkdir $clientcachedir

# sleep 1

echo "start electron on nodes"
for row in {1..5}
  do
    echo  {\"name\": \"Wild\",\"main\": \"$nodetmppath/electronmain.js\",\"launch\": "\"http://$serverip:$serverport/?id=$col$row&offsetx=$offsetX&offsety=$offsetY&wallx=$wcol&wally=$wrow&wallwidth=$wallwidth&wallheight=$wallheight&sizex=$wtile_w&sizey=$wtile_h&syncserver=$serverip&viewSyncPort=$viewSyncPort\"",\"window\": {\"kiosk\": true,\"width\": $wtile_w,\"height\": $wtile_h}} > $serverpackagepath$row
    scp $serverpackagepath$row $user@c$row:$nodetmppath/$packagefile
    scp $electronstartscript $user@c$row:$nodetmppath/
    scp $electronpreloadscript $user@c$row:$nodetmppath/
    ssh $user@c$row "cd $nodetmppath ; DISPLAY=:0 $electronpath . "&
    sleep 0.2
    offsetY=$((offsetY+wtile_h))
  done
  offsetX=$((offsetX+wtile_wa))
  offsetY=0
  wtile_w=$wtile_wb


sleep 2

echo  {\"name\": \"Wild\",\"main\": \"electronmain.js\",\"launch\": "\"http://$serverip:$serverport/?id=MASTER&wallwidth=$wallwidth&wallheight=$wallheight&sizex=$masterwidth&sizey=$masterheight&syncserver=$serverip&viewSyncPort=$viewSyncPort\"",\"window\": {\"kiosk\": false,\"width\": $masterwidth,\"height\": $masterTotalHeight}} > package.json
$electronpath .
