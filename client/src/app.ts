import {WildOsdOptions, WildOsd, PieMenu, FixedOverlay} from "wildosd";
import randomColor from "randomcolor";


const queryString = window.location.search;
const urlParams = new URLSearchParams(queryString);

console.log(`url parameters ${urlParams}`);

let id: string = "";


let pieMenuRadius: number;
let pieMenuSize: number = 5; //Percent of the height of the wall

let syncServerIp = "127.0.0.1";
if (urlParams.has('syncserver')) {
	syncServerIp = <string>urlParams.get('syncserver');
}

let viewSyncPort = 8080;
if (urlParams.has('viewSyncPort')) {
	viewSyncPort = +<string>urlParams.get('viewSyncPort');
}


let tuioServerIp = "127.0.0.1";
if (urlParams.has('tuioserver')) {
	tuioServerIp = <string>urlParams.get('tuioserver');
}

let tuioPort = 8083;
if (urlParams.has('tuioport')) {
	tuioPort = +<string>urlParams.get('tuioport');
}


let offsetX: number = 1;
if (urlParams.has('offsetx')) {
	// @ts-ignore
	offsetX = +urlParams.get('offsetx');
}
let offsetY: number = 1;
if (urlParams.has('offsety')) {
	// @ts-ignore
	offsetY = +urlParams.get('offsety');
}


let sizeX: number = 800.0;
let sizeY: number = 600.0;

if (urlParams.has('sizex')) {
	// @ts-ignore
	sizeX = +urlParams.get('sizex');
}
if (urlParams.has('sizey')) {
	// @ts-ignore
	sizeY = +urlParams.get('sizey');
}

let wallWidth: number = 800.0;
let wallHeight: number = 600.0;

if (urlParams.has('wallwidth')) {
	// @ts-ignore
	wallWidth = +urlParams.get('wallwidth');
}
if (urlParams.has('wallheight')) {
	// @ts-ignore
	wallHeight = +urlParams.get('wallheight');
}

//Set nw position
if (urlParams.has('posx')) {
	// @ts-ignore
	let posx = +urlParams.get('posx');
	console.log("posx " + posx);
	WildOsd.setWindowPosition(posx, 0);
}


let el = document.getElementById("seadragon-viewer");
if (el) {
	el.style.width = `${sizeX}px`;
	el.style.height = `${sizeY}px`;

	console.log(`Canvas size is ${sizeX}px x ${sizeY}px`);
} else {
	el = document.body;
	console.error('Cannot find elem seadragon-viewer');
}


if (urlParams.has('id')) {
	// @ts-ignore
	id = urlParams.get('id');
}

let wildOsdOptions: WildOsdOptions =
	{
		id: id,
		useWebGl: true,
		syncServerIp: syncServerIp,
		syncServerPort: viewSyncPort,
		wallWidth: wallWidth,
		wallHeight: wallHeight,
		tileWidth: sizeX,
		tileHeight: sizeY,
		tileOffsetX: offsetX,
		tileOffsetY: offsetY,
		tuioServerIp: tuioServerIp,
		tuioServerPort: tuioPort,
		clickDistThreshold: 50,
		dblClickDistThreshold: 50,
		osdTileSource: "http://192.168.2.5:8888/Sanctuary2025/Disc_1_matter.dzi"
};

let wildOsd = new WildOsd("Main", el, wildOsdOptions);


//Load bar
if (wildOsd.isMaster) {
	let form = document.createElement("form");
	form.id = "form";
	let inputText = document.createElement("input");
	inputText.setAttribute('list','dzi');
	inputText.setAttribute('size','100');
	let inputButton = document.createElement("input");
	let dataList = document.createElement("datalist");
	dataList.setAttribute('id','dzi');



	let choices = ["http://192.168.2.5:8888/Sanctuary2025/Disc_1_matter.dzi","http://192.168.2.5:8888/Sanctuary2025/Disc_2_space.dzi","http://192.168.2.5:8888/Sanctuary2025/Disc_3_water_k.dzi","http://192.168.2.5:8888/Sanctuary2025/Disc_4_life_k.dzi","http://192.168.2.5:8888/Sanctuary2025/Disc_1_matter_k.dzi","http://192.168.2.5:8888/Sanctuary2025/Disc_3_water.dzi","http://192.168.2.5:8888/Sanctuary2025/Disc_4_life.dzi","http://192.168.2.5:8888/Sanctuary2025/Disc_5_Time.dzi","http://192.168.2.5:8888/Sanctuary/disc1-512.dzi","http://192.168.2.5:8888/Sanctuary/disc3-512.dzi","http://192.168.2.5:8888/Sanctuary/genome-female-1.dzi","http://192.168.2.5:8888/Sanctuary/genome-male-1.dzi","http://192.168.2.5:8888/Sanctuary/disc2-512.dzi","http://192.168.2.5:8888/Sanctuary/disc4-512.dzi","http://192.168.2.5:8888/Sanctuary/genome-female-2.dzi","http://192.168.2.5:8888/Sanctuary/genome-male-2.dzi","rijks"];
	for (const choice of choices) {
		let option = document.createElement("option");
		option.value = choice;
		dataList.appendChild(option);
	}

	inputButton.type = "button";
	inputButton.value = "Load";

	form.appendChild(inputText);
	form.appendChild(inputButton);
	form.appendChild(dataList);

	document.body.appendChild(form);


	inputButton.onclick = () => {
		console.log("LOAD " + inputText.value);
		open(inputText.value);

	}
}

function open(url: string) {
	let tileSource;

	if (url.endsWith("png") || url.endsWith("jpg")) {
		tileSource = {
			tileSource: {
				url: url,
				type: "image",
				buildPyramid: false,
			}
		};
		//tileSource.tileSource.type = "image";
	} else
		tileSource = url;


	console.log("Opening: " + JSON.stringify(tileSource));

	osdOpen(tileSource);

	if (wildOsd.isMaster) {
		wildOsd.rpCall('osdOpen', [tileSource]);

	}
}

function osdOpen(tileSource: string) {
	let source;
	if (tileSource == "rijks") {
		source = {
			tileSource: {
				height: 775000,
				width: 925000,
				tileSize: 1026,
				minLevel: 9,
				maxLevel: 20,
				tileOverlap: 0,
			}
		};

		let tileFunc = function (z, x, y) {
			return "http://192.168.2.254:8086/ERwIn/" +
				z + "/" + x + "_" + y + ".jpeg";
		};
		source.tileSource.getTileUrl = tileFunc;
	} else
		source = tileSource;

	console.log(source);

	wildOsd.osdViewer.open(source);
	init();
}

wildOsd.rpRegister("osdOpen", osdOpen);


//PIE MENU
if (wildOsd.isMaster) {
	pieMenuRadius = pieMenuSize * wallHeight / 100 * sizeX / wallWidth;
} else {
	pieMenuRadius = pieMenuSize * wallHeight / 100;
}
let pieMenu;


let menuOverlay;

let dragMagId = 0;

function createDragMagClient(rootId: string, id: string, color: string, x: number, y: number, width: number, height: number) {

	// Add new dragmag before menu div
	let dragMagViewer = wildOsd.addDragMagOsd(rootId, id, color, wildOsdOptions, x - wildOsdOptions.tileOffsetX, y - wildOsdOptions.tileOffsetY, width, height);

}

wildOsd.rpRegister("createDragMagClient", createDragMagClient);


function removeDiv(divId: string) {
	//console.log("removeDiv "+divId);
	let divToDestroy = document.getElementById(divId);
	if (divToDestroy) {
		divToDestroy.remove();
	}
}

wildOsd.rpRegister("removeDiv", removeDiv);


wildOsd.osdViewer.addHandler('canvas-click', function (viewerEvent) {
	if (viewerEvent.position) {
		let elements = document.elementsFromPoint(viewerEvent.position.x, viewerEvent.position.y);
		for (let i = 0; i < elements.length; i++) {
			let element = elements[i];
			if (element.getAttribute("class") == PieMenu.pieSliceClassName) {
				pieMenu.click(element.id);
				return;
			}
		}
	}
});


wildOsd.osdViewer.addHandler('canvas-key', function (viewerEvent) {
	if (viewerEvent['originalEvent']['type'] == 'keypress') {
		if (viewerEvent['originalEvent']['key'] == 'c') {
			console.log("Clear touch events");
			wildOsd.resetTouch();
		}
	}
});

//TODO move it in WildOsd
function pieMenuShow(x: number, y: number) {
	menuOverlay.setViewportPosition(x, y);
	pieMenu.show();
	wildOsd.osdViewer.forceRedraw()
	if (wildOsd.isMaster)
		wildOsd.rpCall("pieMenuShow", [x, y]);
}

wildOsd.rpRegister("pieMenuShow", pieMenuShow);

function pieMenuHide() {
	pieMenu.hide();
	wildOsd.osdViewer.forceRedraw()
	if (wildOsd.isMaster)
		wildOsd.rpCall("pieMenuHide", []);
}

wildOsd.rpRegister("pieMenuHide", pieMenuHide);

function pieMenuSetLayer(layerID: string) {
	pieMenu.setCurrentLayer(layerID);
	wildOsd.osdViewer.forceRedraw();
	if (wildOsd.isMaster)
		wildOsd.rpCall("pieMenuSetLayer", [layerID]);
}

wildOsd.rpRegister("pieMenuSetLayer", pieMenuSetLayer);

function init() {

	wildOsd.osdViewer.addHandler('canvas-double-click', function (viewerEvent) {
		if (viewerEvent.position) {
			let viewportPos = wildOsd.osdViewer.viewport.viewerElementToViewportCoordinates(viewerEvent.position);
			pieMenuShow((viewerEvent.position.x - pieMenuRadius) / sizeX, (viewerEvent.position.y - pieMenuRadius) / sizeY);
		}
	})

	pieMenu = new PieMenu(pieMenuRadius);

	menuOverlay = new FixedOverlay(wildOsd, wildOsd.overlayRootDiv, pieMenu.root);


	pieMenu.addSlice('#222', 50, "Remove", () => {
		let x = parseInt(pieMenu.root!.style.left) + pieMenu.r;
		let y = parseInt(pieMenu.root!.style.top) + pieMenu.r;
		let windowName = wildOsd.removeWindowByPosition(x, y);
		pieMenuSetLayer(PieMenu.pieMenuDefaultLayer);
		pieMenuHide();
	});


	pieMenu.addSlice('#222', 50, "Add Dragmag", () => {

		let x = parseInt(pieMenu.root!.style.left) + pieMenu.r;
		let y = parseInt(pieMenu.root!.style.top) + pieMenu.r;

		let w = 200;
		let h = 200

		let color = randomColor();

		// Add new dragmag before menu div
		let dragMagViewer = wildOsd.addDragMagOsd(pieMenu.root.id, "dragmag" + dragMagId, color, wildOsdOptions, x, y, w, h);


		if (wildOsd.isMaster) {
			let rect = wildOsd.getWallPosition(x, y, w, h);
			wildOsd.rpCall('createDragMagClient', [pieMenu.root.id, "dragmag" + dragMagId, color, rect.x, rect.y, rect.w, rect.h]);
		}
		pieMenuSetLayer(PieMenu.pieMenuDefaultLayer);
		pieMenuHide();
		dragMagId++;

	});


	pieMenu.init();
	pieMenu.hide();

	// Add Inria Logo to the top right hand corner
	let logoDiv = document.createElement('div') as HTMLDivElement;
	let logoInria = document.createElement('img') as HTMLImageElement;
	logoDiv.appendChild(logoInria);
	logoInria.src = "images/logo_inria.svg";
	logoInria.id = "logoInria";

	let logoOverlay = new FixedOverlay(wildOsd, wildOsd.overlayRootDiv, logoInria);
	logoOverlay.setViewportPosition(0.9, 0.0);
	logoOverlay.setViewportSize(0.1, 0.1);

}

init();


