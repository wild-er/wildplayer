const path = require('path');

module.exports = {
	watch: false,
	entry: path.join(__dirname, './src/app.ts'),
	mode: 'development',
	devtool: 'source-map',
	output: {
		filename: 'bundle.js',
		sourceMapFilename: "bundle.js.map",
		path: path.resolve(__dirname, './dist/scripts'),
		library: "EntryPoint"
	},
	resolve: {
		extensions: ['.ts', '.tsx', '.js', '.jsx', '.json']
	},
	module: {
		rules: [
			{
				test: /\.(js|jsx|tsx|ts)$/,
				exclude: /node_modules/,
				use: [
					{
						loader: 'ts-loader',
						options: {
							configFile: path.resolve('./tsconfig.json'),
						},
					},
				],
			}
		]
	}
};
